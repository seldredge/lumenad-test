import time
import requests
from selenium import webdriver
from bs4 import BeautifulSoup


def test_login():
    driver = webdriver.Chrome('/Applications/chromedriver')

    #load the page 
    driver.get('https://develop.lumenad.com/login')

    #show page for demo 
    time.sleep(3)
    #Use this to find all input elements on page 
    # print(x.findAll('input', {'type':'text'}))

    x = driver.page_source
    inputs = driver.find_elements_by_tag_name('input')
    
    #Test that page loaded correctly (log in elements exist) 
    assert inputs, 'No inputs returned'
    assert len(inputs) >= 2, 'number of inputs not 2, page not loaded correctly'
    #Enter User Info and log in
    uname = "seldredge@lumenad.com"
    pword = "YoSUR4T0CGn4"

    inputs[0].send_keys(uname)
    inputs[1].send_keys(pword)
    time.sleep(2)
    click_me = driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[2]/div/div[1]/div[2]/div/div[1]/div/form/div/div/button/span[1]')
    click_me.submit()
    time.sleep(3)

    #test that log in happened correctly 
    lumenadHeader = driver.find_element_by_xpath('//*[@id="page-content-wrapper"]/div/div/div/div/div[1]/div/h5')
    assert lumenadHeader, 'Unable to locate element in header'
    
    driver.close()

