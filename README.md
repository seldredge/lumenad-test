# README #

This repo is intended to be a general sandbox for all automated tests related to QA.   
It's pretty barren right now, so check back often to see the new and exciting stuff your QA team is working on!   


## Getting Started ##

If you already know your way around here the easiest way to get started is to pull this repo to your local machine. 
Make sure you have python 3.9 or above, and run the command ```pip install pipenv```
Once you have a local clone, run ```pipenv install``` to create a virtual environment, then run ```pipenv run pytest```


For more detailed instructions, scroll down to "How do I make this work?" 

All tests in this repo are designed to run together using pytest. Woah, only one command?! Yup, simple.     

### When should I run these tests? ###

The intended use case is to run these tests after you push something new to Develop. 
Tests can be run as a suite or individually to tame those acute feelings of post-push paranoia. 

### What if I need a test that isnt here? ###

We are still in the alpha phase, so this is to be expected! 
If you have any test requests, shoot a message to one of your QAs, and we will work to make it happen. 
(or come up with a suitable excuse) 

### How can I get involved with this project? ###
Whether because of excitement or frustration, we are super happy you want to contribute! 
To submit your own tests to the project, please read through the contribution guidelines below. 
You should probably also check with your team's PM first, to make sure that this is a good way to allocate your time. 
But then again, I'm a ReadMe, not a cop. 


## How do I make this thing work? ##

In order for these tests to run on your computer, you will need to make sure a few things are configured correctly. 

1. Make sure you have python installed
This should come pre-installed on your mac, but if you have another device or you arent sure that you have python, run the below command to check the version. 
```python --version ```

You will need at least a version of 2.9.something or later, but you can also upgrade to python3 for extra street cred.  
If you're coming up totally empty, please take a moment to consider where you went wrong in life, and then get yourself a shiny new download. 
https://www.python.org/downloads/mac-osx/

Speaking of python on the Mac, there is some configuration weirdness that means they dont always play nicely. 
If you're having trouble using the pip installer, try reading through this page.
https://packaging.python.org/tutorials/installing-packages/

If you don't feel like reading through documentation, you can try replacing all of the below pip commands with 'pip3' commands. 


2. The virtual environment should manage all of the configurtion issues for you, so you wont need to install the various packages used in the tests seperately. However, if you are interested in playing with selenium locally, you'll want to pip install the following: 

```pip install selenium``` 

```pip install pytest```

```pip install requests```

```pip install bs4```

3. You shouldn't need a seperate download for the Chrome Webdriver

However, If you're running into trouble related to your driver, check that the version matches the version of your browser. 

You might also encounter an error related to the driver beloning to an unidentified developer. If that is the case: 
	
	-In the Finder on your Mac, locate the app you want to open.
	
	-Don’t use Launchpad to do this. Launchpad doesn’t allow you to access the shortcut menu.
	
	-Control-click the app icon, then choose Open from the shortcut menu.
	
	-Click Open.
	
	-The app is saved as an exception to your security settings, and you can open it in the future by double-clicking it just as you can any registered app.

## Contribution guidelines ##

As of April 2021, this repo is designed to support tests written in python. All new contributions should follow the naming convention
"test_TestName" this handy dandy convention lets your test be automatically picked up by pytest, meaning its one step to run all your tests together. 

## Who do I talk to? ##

All questions about this project can be directed to Sarah, who will likely need to google the answers. 
And uhh, I hear you should probably call your mother more often. 
